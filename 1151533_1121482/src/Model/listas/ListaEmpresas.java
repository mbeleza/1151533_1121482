package Model.listas;

import Model.Empresa;
import java.io.Serializable;
import java.util.ArrayList;

public class ListaEmpresas implements Serializable
{
    private ArrayList<Empresa> listaEmpresas;

    public ListaEmpresas()
    {
        this.listaEmpresas = new ArrayList<>();
    }

    public ArrayList<Empresa> getEmpresas()
    {
        return this.listaEmpresas;
    }

    /**
     * Verificar se existe alguma empresa com o nome "strNome"
     * @param strNome Nome a ser verificado
     * @param emp Empresa a não verificar, caso esteja a editar "emp"
     * @return True se existir alguma empresa com o nome pedido, excluindo "emp"
     */
    public boolean verificarNome(String strNome, Empresa emp){
        for(Empresa e : this.getEmpresas()){
            if(e.getNome().equalsIgnoreCase(strNome) && !e.equals(emp))
                return true;
        }
        return false;
    }

    /**
     * Verificar se existe alguma empresa com a morada "strMorada"
     * @param strMorada Morada a ser verificado
     * @param emp Empresa a não verificar, caso esteja a editar "emp"
     * @return True se existir alguma empresa com a morada pedida, excluindo "emp"
     */
    public boolean verificarMorada(String strMorada, Empresa emp){
        for(Empresa e : this.getEmpresas()){
            if(e.getMorada().equals(strMorada) && !e.equals(emp))
                return true;
        }
        return false;
    }

    /**
     * Verificar se existe alguma empresa com o nipc "strNipc"
     * @param strNipc Nome a ser verificado
     * @param emp Empresa a não verificar, caso esteja a editar "emp"
     * @return True se existir alguma empresa com o nipc pedido, excluindo "emp"
     */
    public boolean verificarNipc(int strNipc, Empresa emp){
        for(Empresa e : this.getEmpresas()){
            if(e.getNipc() == strNipc && !e.equals(emp))
                return true;
        }
        return false;
    }

    /**
     * Verificar se existe alguma empresa com o telefone "intTelefone"
     * @param intTelefone Nome a ser verificado
     * @param emp Empresa a não verificar, caso esteja a editar "emp"
     * @return True se existir alguma empresa com o telefone pedido, excluindo "emp"
     */
    public boolean verificarTelefone(int intTelefone, Empresa emp){
        for(Empresa e : this.getEmpresas()){
            if(e.getTelefone() == intTelefone && !e.equals(emp))
                return true;
        }
        return false;
    }

    /**
     * Remover Empresa escolhida
     * @param e Empresa a remover
     */
    public void removerEmpresa(Empresa e)
    {
        int index = -1;
        for(Empresa emp : this.listaEmpresas){
            index++;
            if(emp.equals(e))
                this.listaEmpresas.remove(index);
        }
    }
}
