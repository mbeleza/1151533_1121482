package Model.listas;

import Model.Curso;
import Model.Estudante;
import java.io.Serializable;
import java.util.ArrayList;

public class ListaEstudantes implements Serializable
{
    private ArrayList<Estudante> listaEstudantes;

    public ListaEstudantes()
    {
        this.listaEstudantes = new ArrayList<>();
    }

    public ArrayList<Estudante> getEstudantes()
    {
        return this.listaEstudantes;
    }

    /**
     * Obter número mecanográfico sequencial
     * @param c Objeto do Curso para obter ano letivo
     * @return número mecanográfico
     */
    public int mecanografico(Curso c)
    {
        if(this.listaEstudantes.size() > 0)
            return this.listaEstudantes.get(this.listaEstudantes.size()-1).getMecanografico() + 1;
        else
            return (int) (c.getAnoLectivo()*Math.pow(10,4) + 12*100 +1);
    }

    /**
     * Obter Alunos com nota igual ou superior à pedida
     * @param nota Nota pedida
     */
    public String getNotaMaisAlta(int nota)
    {
        int index = 0;
        int nota_aluno;
        String lista = "";
        for(Estudante e : this.listaEstudantes)
            if ((nota_aluno = e.getClassificacao()) >= nota){
                index++;
                lista += index + ". " + e.getNome() + " (" + nota_aluno + ")\n";
            }

        if(index > 0)
            return lista;
        else
            return "Não há nenhum aluno com nota igual ou superior a " + nota + ".";
    }

    /**
     * Verificar se existe algum estudante com o nome "strNome"
     * @param strNome Nome a ser verificado
     * @param est Estudante a não verificar, caso esteja a editar "est"
     * @return True se existir alguma estudante com o nome pedido, excluindo "est"
     */
    public boolean verificarNome(String strNome, Estudante est){
        for(Estudante e : this.listaEstudantes){
            if(e.getNome().equalsIgnoreCase(strNome) && !e.equals(est))
                return true;
        }
        return false;
    }

    /**
     * Verificar se existe alguma estudante com o cc "strCc"
     * @param intCc CC a ser verificado
     * @param est Estudante a não verificar, caso esteja a editar "est"
     * @return True se existir alguma Estudante com o cc pedido, excluindo "est"
     */
    public boolean verificarCc(int intCc, Estudante est){
        for(Estudante e : this.listaEstudantes){
            if(e.getCc() == intCc && !e.equals(est))
                return true;
        }
        return false;
    }

    /**
     * Verificar se existe alguma estudante com o nif "strCc"
     * @param intNif Fif a ser verificado
     * @param est Estudante a não verificar, caso esteja a editar "est"
     * @return True se existir alguma Estudante com o nif pedido, excluindo "est"
     */
    public boolean verificarNif(int intNif, Estudante est){
        for(Estudante e : this.listaEstudantes){
            if(e.getNif() == intNif && !e.equals(est))
                return true;
        }
        return false;
    }

    /**
     * Remover Estudante da lista
     * @param e Estudante a remover
     */
    public void removerEstudante(Estudante e)
    {
        int index = -1;
        for(Estudante est : this.listaEstudantes){
            index++;
            if(est.equals(e))
                this.listaEstudantes.remove(index);
        }
    }

}
