package Model.utils;

import Model.Curso;
import java.io.*;

public class Ficheiro
{
    /**
     * Leitura do ficheiro relativo ao curso, se não existir, cria um novo
     * @return Curso criado a partir do ficheiro lido
     */
    public static Curso lerInformacaoCurso()
    {
        Curso c = new Curso();
        try
        {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("Curso.bin"));
            c = (Curso) in.readObject();
            in.close();
            System.out.println("\nLeitura de ficheiro concluida...");
        }
        catch (ClassNotFoundException | ClassCastException  | StreamCorruptedException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            try
            {
                ObjectOutputStream out = new ObjectOutputStream( new FileOutputStream("Curso.bin") );
                out.close();
                System.out.println("\nFicheiro Curso.bin criado...");
            }
            catch(IOException j)
            {
                j.printStackTrace();
            }
        }
        return c;
    }

    /**
     * Guardar informações do Curso para o ficheiro
     * @param c Curso a guardar
     * @throws IOException
     */
   public static void guardaInformacaoCurso(Curso c) throws IOException
   {
        ObjectOutputStream out = new ObjectOutputStream( new FileOutputStream("Curso.bin") );
        try
        {
            out.writeObject(c);
            out.flush();
            out.close();
            System.out.println("\nEscrita para ficheiro concluida...");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}