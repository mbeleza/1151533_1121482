package Model.listas;

import Model.Orientador;
import java.io.Serializable;
import java.util.ArrayList;

public class ListaOrientadores implements Serializable
{
    private ArrayList<Orientador> listaOrientadores;

    public ListaOrientadores()  
    {
        this.listaOrientadores = new ArrayList<>();
    }

    public ArrayList<Orientador> getOrientadores()
    {
        return this.listaOrientadores;
    }

    /**
     * Verificar se existe algum orientador com o nome "strNome"
     * @param strNome Nome a ser verificado
     * @param est Orientador a não verificar, caso esteja a editar "est"
     * @return True se existir alguma orientador com o nome pedido, excluindo "est"
     */
    public boolean verificarNome(String strNome, Orientador est)
    {
        for(Orientador e : this.listaOrientadores){
            if(e.getNome().equalsIgnoreCase(strNome) && !e.equals(est))
                return true;
        }
        return false;
    }

    /**
     * Verificar se existe alguma orientador com o cc "strCc"
     * @param intCc CC a ser verificado
     * @param est Orientador a não verificar, caso esteja a editar "est"
     * @return True se existir alguma Orientador com o cc pedido, excluindo "est"
     */
    public boolean verificarCc(int intCc, Orientador est)
    {
        for(Orientador e : this.listaOrientadores){
            if(e.getCc() == intCc && !e.equals(est))
                return true;
        }
        return false;
    }

    /**
     * Verificar se existe alguma orientador com o nif "strCc"
     * @param intNif Nif a ser verificado
     * @param est Orientador a não verificar, caso esteja a editar "est"
     * @return True se existir alguma Orientador com o nif pedido, excluindo "est"
     */
    public boolean verificarNif(int intNif, Orientador est)
    {
        for(Orientador e : this.listaOrientadores){
            if(e.getNif() == intNif && !e.equals(est))
                return true;
        }
        return false;
    }

    /**
     * Remover Orientador da lista
     * @param o Orientador a remover
     */
    public void removerOrientador(Orientador o)
    {
        int index = -1;
        for(Orientador ori : this.listaOrientadores){
            index++;
            if(ori.equals(o))
                this.listaOrientadores.remove(index);
        }
    }
}
