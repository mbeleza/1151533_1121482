package UI;

import Model.*;
import Model.utils.*;
import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.Serializable;

public class Menu implements Serializable {

    private Curso c;

    public Menu(Curso c)
    {
        this.c = c;
    }

    /**
     * Menu principal e inicial da aplicacaoo
     * @return false se o utilizador decidir encerrar o programa
     */
    public boolean listarMenu()
    {
        System.out.println("\n==[Menu]==");
        int opt = Utils.lerInt(menu(1), 0, 3, 1,1 );
        if(opt!=0) this.executar(opt);
        return false;
    }

    /**
     * Executa uma das opções principais do listarMenu
     * @param opt recebe opção em listarMenu
     */
    private void executar(int opt)
    {
        switch(opt){
            case 1: inserir(); break;
            case 2: listar(); break;
            case 3: gerir(); break;
            default:
                Utils.erroOp();
        }
        this.listarMenu();
    }

    /**
     * Insere objetos nas listas a escolher no metodo
     */
    private void inserir()
    {
        System.out.println("\n==[Inserir]==");
        int opt = Utils.lerInt(menu(3), 0, 5, 1, 1);
        if (opt != 0){
            switch (opt) {
                case 1:
                    this.addEmpresa();
                    break;
                case 2:
                    this.addPessoa("estudante");
                    break;
                case 3:
                    if(this.c.getListaEmpresas().getEmpresas().size() > 0)
                        this.addPessoa("supervisor");
                    else System.out.println("Não pode inserir supervisores sem haver empresas registadas.\n");
                    break;
                case 4:
                    this.addPessoa("orientador");
                    break;
                case 5:
                    if(this.c.getListaOrientadores().getOrientadores().size() > 0)
                        this.addProjeto();
                    else System.out.println("Não pode inserir projetos sem haver orientadores registados.\n");
                    break;
                default: Utils.erroOp();
            }
            this.inserir();
        }
    }

    /**
     * Apresenta os objetos todos da lista selecionada
     */
    private void listar()
    {
        System.out.println("\n==[Listar]==");
        int opt = Utils.lerInt(menu(3), 0, 5, 1, 1);
        if(opt != 0){
            switch(opt){
                case 1:
                    Utils.apresentaLista(this.c.getListaEmpresas().getEmpresas(), "\n==[Lista de Empresas Registadas]==");
                    break;
                case 2:
                    Utils.apresentaLista(this.c.getListaEstudantes().getEstudantes(), "\n==[Lista de Estudantes Registados]==");
                    break;
                case 3:
                    Utils.apresentaLista(this.c.getListaSupervisores().getSupervisores(), "\n==[Lista de Supervisores Registados]==");
                    break;
                case 4:
                    Utils.apresentaLista(this.c.getListaOrientadores().getOrientadores(), "\n==[Lista de Orientadores Registados]==");
                    break;
                case 5:
                    Utils.apresentaLista(this.c.getListaProjetos().getProjetos(), "\n==[Lista de Projetos Registados]==");
                    break;
                default: Utils.erroOp();
            }
            System.out.println();
            listar();
        }
    }

    /**
     * Escolher um objeto a gerir de uma das listas da classe Curso
     */
    private void gerirListas()
    {
        System.out.println("\n==[Gerir Listas]==");
        int opt = Utils.lerInt(menu(3), 0, 5, 1, 1);
        if(opt != 0) {
            switch (opt) {
                case 1:
                    Empresa emp = (Empresa) Utils.apresentaESeleciona(this.c.getListaEmpresas().getEmpresas(), "\n==[Lista de Empresas]==");
                    if (emp != null) this.gerirEmpresa(emp);
                    break;
                case 2:
                    Estudante est = (Estudante) Utils.apresentaESeleciona(this.c.getListaEstudantes().getEstudantes(), "\n==[Lista de Estudantes]==");
                    if(est!=null) this.gerirPessoa(est);
                    break;
                case 3:
                    Supervisor s = (Supervisor) Utils.apresentaESeleciona(this.c.getListaSupervisores().getSupervisores(), "\n==[Lista de Supervisores]==");
                    if(s!=null) this.gerirPessoa(s);
                    break;
                case 4:
                    Orientador o = (Orientador) Utils.apresentaESeleciona(this.c.getListaOrientadores().getOrientadores(), "\n==[Lista de Orientadores]==");
                    if(o!=null) this.gerirPessoa(o);
                    break;
                case 5:
                    Projeto p = (Projeto) Utils.apresentaESeleciona(this.c.getListaProjetos().getProjetos(), "\n==[Lista de Projetos]==");
                    if(p!=null) this.gerirProjeto(p);
                    break;
                default: Utils.erroOp();
            }
            this.gerirListas();
        }
    }

    /**
     * Menu Gerir
     * Responsavel por gerir o programa
     */
    private void gerir()
    {
        System.out.println("\n==[Gerir]==");
        int opt = Utils.lerInt(menu(2), 0, 3 , 1, 1);
        if(opt != 0) {
            switch (opt) {
                case 1:
                    this.gerirListas();
                    break;
                case 2:
                    if(this.c.getListaOrientadores().getOrientadores().size() > 0)
                        this.projetosOrientador();
                    else System.out.println("Não existem orientadores registados!");
                    break;
                case 3:
                    if(this.c.getListaEmpresas().getEmpresas().size() > 0)
                        this.empresaMaisProjetos();
                    else System.out.println("Não existem empresas registados!");
                    break;
                case 4:
                    if(this.c.getListaEstudantes().getEstudantes().size() > 0)
                        this.estudanteNotaMaisAlta();
                    else System.out.println("Não existem estudantes registados!");
                    break;
                default: Utils.erroOp();
            }
            this.gerir();
        }
    }

    /**
     * Menus Gerais
     * @param type tipo de menu
     * @return menu correspondente ao tipo escolhido
     */
    private String menu(int type)
    {
        if(type == 1)
            return "1. Inserir\n2. Listar\n3. Gerir\n0. Sair";
        else if(type == 2)
            return "1. Gerir Listas\n2. Projetos orientados por um docente\n3. Empresa com mais projetos\n4. Aluno com nota igual ou superior à pedida\n0. Regressar";
        else
            return "1. Empresa\n2. Estudante\n3. Supervisor\n4. Orientador\n5. Projeto\n0. Regressar";
    }

    /*
    *
    * Menus referentes a Empresa
    *
    */

    /**
     * Adicionar Empresa à lista
     */
    private void addEmpresa()
    {
        System.out.println("\n==[Inserir Nova Empresa]==");
        String nome = Utils.lerLinha("Nome:");
        while(this.c.getListaEmpresas().verificarNome(nome, null)) {
            System.out.println("Já há uma empresa registada com esse nome!");
            nome = Utils.lerLinha("Introduza outro nome:");
        }
        String morada = Utils.lerLinha("Morada:");
        while(this.c.getListaEmpresas().verificarMorada(morada, null)) {
            System.out.println("Já há uma empresa registada com essa morada!");
            morada = Utils.lerLinha("Introduza outra morada:");
        }
        int nipc = Utils.lerInt("Número de identificação de pessoa coletiva:", 500000000, 999999999, 9, 9);
        while(this.c.getListaEmpresas().verificarNipc(nipc, null)) {
            System.out.println("Já há uma empresa registada com esse NIPC!");
            nipc = Utils.lerInt("Introduza outro número de identificação de pessoa coletiva:", 500000000, 999999999, 9, 9);
        }
        int telefone = Utils.lerInt("Telefone:", 200000000, 299999999, 9, 9);
        while(this.c.getListaEmpresas().verificarTelefone(telefone, null)) {
            System.out.println("Já há uma empresa registada com esse telefone!");
            telefone = Utils.lerInt("Introduza outro telefone:", 200000000, 299999999, 9, 9);
        }
        this.c.getListaEmpresas().getEmpresas().add(new Empresa(nome, morada, nipc, telefone));
        System.out.println("Empresa inserida com sucesso!\n");
    }

    /**
     * Escolhr o que pretendemos gerir da Empresa escolhida
     * @param e Objeto da classe Empresa
     */
    private void gerirEmpresa(Empresa e)
    {
        String menu = "\n==[Gerir Empresa]==\n";
        menu += "1. Ver\n2. Editar\n3. Supervisores Associados\n4. Projetos Associados\n5. Remover\n0. Regressar";
        int opt = Utils.lerInt(menu, 0, 5, 1, 1);
        if (opt != 0){
            switch (opt) {
                case 1:
                    this.infoEmpresa(e);
                    this.gerirEmpresa(e);
                    break;
                case 2:
                    this.editarEmpresa(e);
                    this.gerirEmpresa(e);
                    break;
                case 3:
                    this.supervisoresEmpresa(e);
                    this.gerirEmpresa(e);
                    break;
                case 4:
                    this.projetosEmpresa(e);
                    this.gerirEmpresa(e);
                    break;
                case 5:
                    if(!this.removerEmpresa(e))
                        this.gerirEmpresa(e);
                    break;
                default:
                    Utils.erroOp();
                    this.gerirEmpresa(e);
            }
        }
    }

    /**
     * Obter informações referentes ao objeto Empresa escolhido
     * @param e Objeto da classe Empresa
     */
    private void infoEmpresa(Empresa e)
    {
        String s = "\n==[Informação da empresa selecionada]==\n";
        s += "Nome: " + e.getNome() + "\n";
        s += "Morada: " + e.getMorada() + "\n";
        s += "Número de Identificação de Pessoa Coletiva: " + e.getNipc() + "\n";
        s += "Telefone: " + e.getTelefone() + "\n";
        System.out.println(s);
    }

    /**
     * Editar os dados pretendidos do objeto escolhido
     * @param e Objeto da classe Estudante, Orientador ou Supervisor
     */
    private void editarEmpresa(Empresa e)
    {
        this.infoPessoa(e);
        String menu = "==[Editar]==\n";
        menu += "1. Nome\n2. Morada\n3. Número de Identificação de Pessoa Coletiva\n4. Telefone\n0. Regressar";
        int editar = Utils.lerInt(menu, 0, 4, 1,1);
        if (editar != 0){
            Empresa before = e;
            switch (editar) {
                case 1:
                    String nome = "";
                    do {
                        String str = Utils.lerLinha("Qual o novo nome que quer atribuir? Não preencher, caso não queira alterar o valor");
                        if (str != null && str.length() > 0) {
                            if (this.c.getListaEmpresas().verificarNome(str, e))
                                System.out.println("Já há uma empresa registada com essa morada!\n");
                            else nome = str;
                        } else nome = "none";
                    }while(nome.equals(""));

                    if (!nome.equals("none") && nome.length() > 0 ) {
                        e.setNome(nome);
                        System.out.println("O nome foi editado com sucesso!\n");
                    } else System.out.println("O nome não foi editado!\n");
                    break;
                case 2:
                    String morada = "";
                    do {
                        String str = Utils.lerLinha("Qual a nova morada que quer atribuir? Não preencher, caso não queira alterar o valor");
                        if (str != null && str.length() > 0) {
                            if (this.c.getListaEmpresas().verificarMorada(str, e))
                                System.out.println("Já há uma empresa registada com essa morada!\n");
                            else morada = str;
                        } else morada = "none";
                    }while(morada.equals(""));

                    if (!morada.equals("none") && morada.length() > 0){
                        e.setMorada(morada);
                        System.out.println("A morada foi editada com sucesso!\n");
                    } else System.out.println("A morada não foi editada!\n");
                    break;
                case 3:
                    int nipc = -1;
                    do{
                        int numero = Utils.lerInt("Qual o novo número de identificação de pessoa coletiva que quer atribuir? Atribuir -1, caso não queira alterar o valor", 100000000,999999999, 9, 9);
                        if(numero != -1){
                            if(this.c.getListaEmpresas().verificarNipc(numero, e))
                                System.out.println("Já há uma empresa registada com esse NIPC!\n");
                             else nipc = numero;
                        } else nipc = -2;
                    }while(nipc == -1);

                    if (nipc > 0){
                        e.setNipc(nipc);
                        System.out.println("O número de identificação de pessoa coletival foi editado com sucesso!\n");
                    } else System.out.println("O número de identificação de pessoa coletiva não foi editado!\n");
                    break;
                case 4:
                    int telefone = -1;
                    do{
                        int numero = Utils.lerInt("Qual o novo telefone que quer atribuir? Atribuir -1, caso não queira alterar o valor", 910000000, 969999999, 9, 9);
                        if(numero != -1) {
                            if (this.c.getListaEmpresas().verificarTelefone(numero, e))
                                System.out.println("Já há uma empresa registada com esse telefone!\n");
                            else telefone = numero;
                        } else telefone = -2;
                    }while (telefone == -1);

                    if (telefone > 0) {
                        e.setTelefone(telefone);
                        System.out.println("O telefone foi editado com sucesso!\n");
                    } else System.out.println("O telefone não foi editado!\n");
                    break;
                default:
                    Utils.erroOp();
            }
            this.c.getListaProjetos().editarEmpresaProjetos(before, e);
            this.c.getListaSupervisores().editarEmpresaSupervisores(before, e);
            this.c.getListaSupervisores().editarEmpresaSupervisores(before, e);
            this.editarEmpresa(e);
        }
    }

    /**
     * Apresentar lista de Supervisores de uma Empresa escolhida
     * @param e Objeto da classe Empresa
     */
    private void supervisoresEmpresa(Empresa e)
    {
        int index = 0;
        System.out.println("\n==[Supervisores associados à empresa escolhida]==");
        for (Supervisor s : this.c.getListaSupervisores().getSupervisores()) {
            if(s.getEmpresa().equals(e))
                index++;
                System.out.println(index + ". " + s);
        }
    }

    /**
     * Apresentar lista de Projetos de uma Empresa escolhida
     * @param e Objeto da classe Empresa
     */
    private void projetosEmpresa(Empresa e)
    {
        int index = 0;
        String strMessage = "";
        for (Projeto p : this.c.getListaProjetos().getProjetos()) {
            if(p.getEmpresa() != null) {
                if (p.getEmpresa().equals(e)) {
                    index++;
                    strMessage += index + ". " + p + "\n";
                }
            } else break;
        }

        if(index > 0) {
            System.out.println("\n==[Projetos associados à empresa escolhida]==");
            System.out.println(strMessage);
        } else System.out.println("Esta empresa não está associada a pelo menos um projeto!\n");
    }

    /**
     * Remover Empresa escolhida da lista
     * @param e Objeto da classe Empresa
     */
    private boolean removerEmpresa(Empresa e)
    {
        if(Utils.confirma("\nPretende remover a empresa " + e.getNome() + "? S - Sim | N - Não"))
        {
            this.c.getListaProjetos().removerEmpresaProjetos(e);
            this.c.getListaSupervisores().removerEmpresaSupervisores(e);
            this.c.getListaEmpresas().removerEmpresa(e);
            System.out.println("A empresa foi eliminada com sucesso!\n");
            return true;
        }
        else {
            System.out.println("A empresa não foi eliminada.\n");
            return false;
        }
    }

    /*
    *
    * Menus referentes a Pessoa
    *
    * */

    /**
     * Adicionar Pessoa à lista Estudante, Orientador ou Supervisor
     */
    private void addPessoa(String pessoa)
    {
        int index = -1;
        Empresa empresa;
        String nascimento = "";
        String nome;
        int cc, nif;
        System.out.println("\n==[Inserir novo " + pessoa + "]==");
        switch(pessoa) {
            case "estudante":
                do {
                    nome = Utils.lerLinha("Nome:");
                    if(nome != null && nome.length() > 0) {
                        if (!this.c.getListaEstudantes().verificarNome(nome, null)) {
                            System.out.println("Já existe um estudante com esse nome registado!\n");
                            nome = "";
                        }
                    } else {
                        System.out.println("Não pode introduzir um nome vazio\n");
                        nome = "";
                    }
                }while(nome.equals(""));

                do {
                    String data = Utils.lerLinha("Data de Nascimento [dia/mês/ano]:");
                    if (data != null && data.length() > 0 )
                        if(Data.verificarData(data))
                            nascimento = data;
                        else System.out.println("A data não é válida!\n");
                }while(nascimento.equals(""));

                do {
                    cc = Utils.lerInt("Cartão de Cidadão:", 10000000, 99999999, 8, 8);
                    if (!this.c.getListaEstudantes().verificarCc(cc, null)) {
                        System.out.println("Já existe um estudante com esse cartão de cidadão registado!\n");
                        cc = 0;
                    }
                }while(cc != 0);

                do{
                    nif = Utils.lerInt("Número de identificação fiscal:", 100000000, 299999999, 9, 9);
                    if (this.c.getListaEstudantes().verificarNif(nif, null)) {
                        System.out.println("Já existe um estudante com esse número de identificação fiscal registado!\n");
                        nif = 0;
                    }
                }while(nif != 0);

                String morada = Utils.lerLinha("Morada:");
                int mecanografico = this.c.getListaEstudantes().mecanografico(this.c);
                this.c.getListaEstudantes().getEstudantes().add(new Estudante(nome, Data.converterString(nascimento), cc, nif, (mecanografico + "@isep.ipp.pt"), morada, mecanografico, -1));
                break;

            case "orientador":
                do {
                    nome = Utils.lerLinha("Nome:");
                    if(nome != null && nome.length() > 0) {
                        if (!this.c.getListaOrientadores().verificarNome(nome, null)) {
                            System.out.println("Já existe um orientador com esse nome registado!");
                            nome = "";
                        }
                    } else {
                        System.out.println("Não pode introduzir um nome vazio!\n");
                        nome = "";
                    }
                }while(nome.equals(""));

                do {
                    String data = Utils.lerLinha("Data de Nascimento [dia/mês/ano]:");
                    if (data != null && data.length() > 0 )
                        if(Data.verificarData(data))
                            nascimento = data;
                        else System.out.println("A data não é válida!\n");
                }while(nascimento.equals(""));

                do {
                    cc = Utils.lerInt("Cartão de Cidadão:", 10000000, 99999999, 8, 8);
                    if (!this.c.getListaOrientadores().verificarCc(cc, null)) {
                        System.out.println("Já existe um orientador com esse cartão de cidadão registado!\n");
                        cc = 0;
                    }
                }while(cc != 0);

                do{
                    nif = Utils.lerInt("Número de identificação fiscal:", 100000000, 299999999, 9, 9);
                    if (this.c.getListaOrientadores().verificarNif(nif, null)) {
                        System.out.println("Já existe um orientador com esse número de identificação fiscal registado!\n");
                        nif = 0;
                    }
                }while(nif != 0);

                String[] names = nome.split(" ");
                String sigla = "";
                for (String name : names)
                    sigla += name.charAt(0);
                if(names.length < 2) sigla += (int) (Math.random()*100);
                this.c.getListaOrientadores().getOrientadores().add(new Orientador(nome, Data.converterString(nascimento), cc, nif, (sigla.toLowerCase() + "@isep.ipp.pt"), sigla.toUpperCase()));
                break;

            case "supervisor":
                do {
                    nome = Utils.lerLinha("Nome:");
                    if(nome != null && nome.length() > 0) {
                        if (!this.c.getListaSupervisores().verificarNome(nome, null)) {
                            System.out.println("Já existe um supervisor com esse nome registado!\n");
                            nome = "";
                        }
                    } else {
                        System.out.println("Não pode introduzir um nome vazio!\n");
                        nome = "";
                    }
                }while(nome.equals(""));

                do {
                    String data = Utils.lerLinha("Data de Nascimento [dia/mês/ano]:");
                    if (data != null && data.length() > 0 )
                        if(Data.verificarData(data))
                            nascimento = data;
                        else System.out.println("A data não é válida!\n");
                }while(nascimento.equals(""));

                do {
                    cc = Utils.lerInt("Cartão de Cidadão:", 10000000, 99999999, 8, 8);
                    if (!this.c.getListaSupervisores().verificarCc(cc, null)) {
                        System.out.println("Já existe um supervisor com esse cartão de cidadão registado!\n");
                        cc = 0;
                    }
                }while(cc != 0);

                do{
                    nif = Utils.lerInt("Número de identificação fiscal:", 100000000, 299999999, 9, 9);
                    if (this.c.getListaSupervisores().verificarNif(nif, null)) {
                        System.out.println("Já existe um supervisor com esse número de identificação fiscal registado!\n");
                        nif = 0;
                    }
                }while(nif != 0);

                String email = Utils.lerEmail("Email:");
                do{
                    String emp = Utils.lerLinha("Qual a nova empresa que pretende atribuir ao supervisor?");
                    if (emp != null && emp.length() > 0)
                        index = Utils.procurar(emp, this.c.getListaEmpresas().getEmpresas());
                }while(index == -1);
                empresa = this.c.getListaEmpresas().getEmpresas().get(index);
                this.c.getListaSupervisores().getSupervisores().add(new Supervisor(nome, Data.converterString(nascimento), cc, nif, email, empresa));
                break;
        }
        System.out.println(pessoa + " inserido com sucesso!\n");
    }

    /**
     * Escolher o que pretendemos gerir do objeto Estudante, Orientador ou Supervisor escolhido
     * @param o Objeto da class Estudante, Orientador ou Supervisor
     */
    private void gerirPessoa(Object o)
    {
        String menu = "\n==[Gerir " + o.getClass().getSimpleName() + "]==";
        menu += "1. Ver\n2. Editar\n3. Projetos Associados\n4. Remover\n0. Regressar";
        int opt = Utils.lerInt(menu, 0, 4, 1, 1);
        if (opt != 0){
            switch (opt) {
                case 1:
                    this.infoPessoa(o);
                    this.gerirPessoa(o);
                    break;
                case 2:
                    this.editarPessoa(o);
                    this.gerirPessoa(o);
                    break;
                case 3:
                    this.projetosPessoa(o);
                    break;
                case 4:
                    if(!this.removerPessoa(o))
                        this.gerirPessoa(o);
                    break;
                default:
                    Utils.erroOp();
                    this.gerirPessoa(o);
            }
        }
    }

    /**
     * Obter informações referentes ao objeto escolhido
     * @param o Objeto da classe Estudante, Orientador ou Supervisor
     */
    private void infoPessoa(Object o)
    {
        String s = "\n==[Informação do " + o.getClass().getSimpleName().toLowerCase() + " selecionado]==\n";

        Pessoa p = (Pessoa) o;
        s += "Nome: " + p.getNome() + "\n";
        s += "Data de Nascimento: " + p.getNascimento() + "\n";
        s += "Cartão de Cidadão: " + p.getCc() + "\n";
        s += "Número de Identificação Fiscal: " + p.getNif() + "\n";
        s += "Email: " + p.getEmail() + "\n";

        if(o instanceof Estudante)
        {
            s += "Morada: " + ((Estudante) o).getMorada() + "\n";
            s += "Número Mecanográfico: " + ((Estudante) o).getMecanografico() + "\n";
            s += "Classificação: " + ((Estudante) o).getClassificacao() + "\n";
        }

        else if(o instanceof Orientador)
            s += "Sigla: " + ((Orientador) o).getSigla() + "\n";

        else
            s += "Empresa Associada: " + ((Supervisor) o).getEmpresa() + "\n";
        System.out.println(s);
    }

    /**
     * Editar os dados pretendidos do objeto escolhido
     * @param o Objeto da classe Estudante, Orientador ou Supervisor
     */
    private void editarPessoa(Object o)
    {
        this.infoPessoa(o);
        String menu = "==[Editar]==";
        menu += "1. Nome\n2. Data de Nascimento\n3. Cartão de Cidadão\n4. Número de Identificação Fiscal\n";
        if(o instanceof Estudante)
            menu += "5. Morada\n";
        else if(o instanceof Supervisor)
            menu += "5. Empresa\n6. Email\n";
        menu += "0. Regressar";
        int editar = Utils.lerInt(menu, 0, 7, 1,1);
        if (editar != 0){
            Object before = o;
            switch (editar) {
                case 1:
                String nome = "";
                do {
                    String str = Utils.lerLinha("Qual o novo nome que quer atribuir? Não preencher, caso não queira alterar o valor");
                    if (str != null && str.length() > 0) {
                        if(o instanceof Estudante) {
                            if (this.c.getListaEstudantes().verificarNome(str, (Estudante) o))
                                System.out.println("Já há um estudante registado com esse nome!\n");
                            else nome = str;
                        }
                        else if(o instanceof Orientador) {
                            if (this.c.getListaOrientadores().verificarNome(str, (Orientador) o))
                                System.out.println("Já há um orientador registado com esse nome!\n");
                            else nome = str;
                        }
                        else if(o instanceof Supervisor) {
                            if (this.c.getListaSupervisores().verificarNome(str, (Supervisor) o))
                                System.out.println("Já há um supervisor registado com esse nome!\n");
                            else nome = str;
                        }
                    } else nome = "none";
                }while(nome.equals(""));

                if (!nome.equals("none") && nome.length() > 0 ) {
                    if(o instanceof Estudante)
                        ((Estudante) o).setNome(nome);
                    else if(o instanceof Orientador)
                        ((Orientador) o).setNome(nome);
                    else if(o instanceof Supervisor)
                        ((Supervisor) o).setNome(nome);
                    System.out.println("O nome foi editado com sucesso!");
                } else System.out.println("O nome não foi editado!\n");
                break;
                case 2:
                    String nascimento = "";
                    do {
                        String data = Utils.lerLinha("Qual a nova data de nascimento que quer atribuir? Não preencher, caso não queira alterar o valor");
                        if (data != null && data.length() > 0){
                            if(Data.verificarData(data))
                                nascimento = data;
                            else System.out.println("A data não é valida!");
                        }else nascimento = "none";
                    }while(nascimento.equals(""));

                    if (!nascimento.equals("none")) {
                        if(o instanceof Estudante)
                            ((Estudante) o).setNascimento(Data.converterString(nascimento));
                        else if(o instanceof Orientador)
                            ((Orientador) o).setNascimento(Data.converterString(nascimento));
                        else if(o instanceof Supervisor)
                            ((Supervisor) o).setNascimento(Data.converterString(nascimento));
                        System.out.println("A data de nascimento foi editada com sucesso!");
                    } else System.out.println("A data de nascimento não foi editada!");
                    break;
                case 3:
                    int cc = -1;
                    do{
                        int numero = Utils.lerInt("Qual o novo cartão de cidadão que quer atribuir? Atribuir -1, caso não queira alterar o valor", 10000000,99999999, 8, 8);
                        if(numero != -1){
                            if(o instanceof Estudante) {
                                if (this.c.getListaEstudantes().verificarCc(numero, (Estudante) o))
                                    System.out.println("Já há um estudante registado com esse cartão de cidadão!\n");
                                else cc = numero;
                            }
                            else if(o instanceof Orientador) {
                                if (this.c.getListaOrientadores().verificarCc(numero, (Orientador) o))
                                    System.out.println("Já há um orientador registado com esse cartão de cidadão!\n");
                                else cc = numero;
                            }
                            else if(o instanceof Supervisor) {
                                if (this.c.getListaSupervisores().verificarCc(numero, (Supervisor) o))
                                    System.out.println("Já há um supervisor registado com esse cartão de cidadão!\n");
                                else cc = numero;
                            }
                        } else cc = -2;
                    }while(cc == -1);

                    if (cc > -1){
                        if(o instanceof Estudante)
                            ((Estudante) o).setCc(cc);
                        else if(o instanceof Orientador)
                            ((Orientador) o).setCc(cc);
                        else
                            ((Supervisor) o).setCc(cc);
                        System.out.println("O cartão de cidadão foi editado com sucesso!");
                    } else System.out.println("O cartão de cidadão não foi editado!");
                    break;
                case 4:
                    int nif = -1;
                    do{
                        int numero = Utils.lerInt("Qual o novo número de identificação fiscal que quer atribuir? Atribuir -1, caso não queira alterar o valor", 100000000,299999999, 9, 9);
                        if(numero != -1){
                            if(o instanceof Estudante) {
                                if (this.c.getListaEstudantes().verificarNif(numero, (Estudante) o))
                                    System.out.println("Já há um estudante registado com esse número de identificação fiscal!\n");
                                else nif = numero;
                            }
                            else if(o instanceof Orientador) {
                                if (this.c.getListaOrientadores().verificarNif(numero, (Orientador) o))
                                    System.out.println("Já há um orientador registado com esse número de identificação fiscal!\n");
                                else nif = numero;
                            }
                            else if(o instanceof Supervisor) {
                                if (this.c.getListaSupervisores().verificarNif(numero, (Supervisor) o))
                                    System.out.println("Já há um supervisor registado com esse número de identificação fiscal!\n");
                                else nif = numero;
                            }
                        } else nif = -2;
                    }while(nif == -1);

                    if (nif > -1){
                        if(o instanceof Estudante)
                            ((Estudante) o).setNif(nif);
                        else if(o instanceof Orientador)
                            ((Orientador) o).setNif(nif);
                        else
                            ((Supervisor) o).setNif(nif);
                        System.out.println("O número de identificação fiscal foi editado com sucesso!");
                    } else System.out.println("O número de identificação fiscal não foi editado!");
                    break;
                case 5:
                    if(o instanceof Estudante) {
                        String morada = Utils.lerLinha("Qual a nova morada que quer atribuir? Não preencher, caso não queira alterar o valor");
                        if (morada != null && morada.length() > 0) {
                            ((Estudante) o).setMorada(morada);
                            System.out.println("A morada foi editada com sucesso!");
                        }
                    }
                    else if(o instanceof Supervisor) {
                        int index;
                        do{
                            String empresa = Utils.lerLinha("Qual a nova empresa que quer atribuir? Não preencher, caso não queira alterar o valor");
                            if (empresa != null && empresa.length() > 0) index = Utils.procurar(empresa, this.c.getListaEmpresas().getEmpresas());
                            else index = -2;
                        }while(index == -1);
                        if(index > -1) {
                            ((Supervisor) o).setEmpresa(this.c.getListaEmpresas().getEmpresas().get(index));
                            System.out.println("A empresa foi editada com sucesso!");
                        }
                        else System.out.println("A empresa não foi editada!");
                    } else Utils.erroOp();
                    break;
                case 6:
                    if(o instanceof Supervisor) {
                        String email = "";
                        do {
                            String str = Utils.lerEmail("Qual o novo email que quer atribuir? Não preencher, caso não queira alterar o valor");
                            if (str != null && str.length() > 0) {
                                if (this.c.getListaSupervisores().verificarEmail(str, (Supervisor) o))
                                    System.out.println("Já há um supervisor registado com esse email!\n");
                                else email = str;
                            } else email = "none";
                        } while (email.equals(""));

                        if (!email.equals("none") && email.length() > 0) {
                            ((Supervisor) o).setEmail(email);
                            System.out.println("O email foi editado com sucesso!");
                        } else System.out.println("O email não foi editado!\n");
                    } else Utils.erroOp();
                    break;
                default:
                    Utils.erroOp();
            }
            this.c.getListaProjetos().editarPessoaProjetos(before, o);
            this.editarPessoa(o);
        }
    }

    /**
     * Apresentar lista de Projetos de um Estudante, Orientador ou Supervisor escolhido
     * @param o Objeto da classe Estudante, Orientador ou Supervisor
     */
    private void projetosPessoa(Object o)
    {
        int index = 0;
        String strMessage = "";
        for (Projeto p : this.c.getListaProjetos().getProjetos()) {
            if(o instanceof Estudante) {
                if(p.getEstudante() != null) {
                    if (p.getEstudante().equals(o)) {
                        index++;
                        strMessage += index + ". " + p + "\n";
                    }
                } else break;
            }
            else if(o instanceof Orientador) {
                if(p.getOrientador() != null) {
                    if (p.getOrientador().equals(o)) {
                        index++;
                        strMessage += index + ". " + p + "\n";
                    }
                } else break;
            }
            else if(o instanceof Supervisor) {
                if(p.getSupervisor() != null) {
                    if (p.getSupervisor().equals(o)) {
                        index++;
                        strMessage += index + ". " + p + "\n";
                    }
                } else break;
            }
        }

        if(index > 0) {
            System.out.println("\n==[Projetos associados ao " + o.getClass().getSimpleName().toLowerCase() + " escolhido]==");
            System.out.println(strMessage);
        } else System.out.println("Este " + o.getClass().getSimpleName().toLowerCase() + " não está associado a pelo menos um projeto!\n");
    }

    /**
     * Remover Estudante, Orientador ou Supervisor escolhido da lista
     * @param o Objeto da classe Estudante, Orientador ou Supervisor
     */
    private boolean removerPessoa(Object o)
    {
        if(Utils.confirma("\nPretende remover o " + o.getClass().getSimpleName().toLowerCase() + " escolhido? S - Sim | N - Não"))
        {
            this.c.getListaProjetos().removerPessoaProjetos(o);
            if(o instanceof Estudante)
                this.c.getListaEstudantes().removerEstudante((Estudante) o);
            else if(o instanceof Orientador)
                this.c.getListaOrientadores().removerOrientador((Orientador) o);
            else if(o instanceof Supervisor)
                this.c.getListaSupervisores().removerSupervisor((Supervisor) o);

            System.out.println("O " + o.getClass().getSimpleName().toLowerCase() + " foi removido com sucesso!\n");
            return true;
        }
        else {
            System.out.println("O " + o.getClass().getSimpleName().toLowerCase() + " não foi removido.\n");
            return false;
        }
    }

    /*
    *
    * Menus referentes a Projetos
    *
    * */

    /**
     * Adicionar Projeto à lista Projetos
     */
    private void addProjeto() {
        Empresa empresa = null;
        Supervisor supervisor = null;
        Estudante estudante = null;
        int index;

        System.out.println("\n==[Inserir Nova Projeto]==");
        String titulo = Utils.lerLinha("Titulo:");
        String descricao = Utils.lerLinha("Descrição:");
        if (this.c.getListaEmpresas().getEmpresas().size() > 0){
            if (Utils.confirma("Pretende atribuir uma empresa a este projeto? S - Sim | N - Não")) {
                do {
                    index = Utils.procurar("Qual a empresa que pretende atribuir ao projeto? ", this.c.getListaEmpresas().getEmpresas());
                } while (index == -1);
                empresa = this.c.getListaEmpresas().getEmpresas().get(index);

                do {
                    index = Utils.procurar("Qual o supervisor que pretende atribuir ao projeto?", this.c.getListaSupervisores().getSupervisores());
                } while (index == -1);
                supervisor = this.c.getListaSupervisores().getSupervisores().get(index);
            }
        }
        if(Utils.confirma("Pretender atribuir um estudante a este projeto? S - Sim | N - Não")) {
            do {
                index = Utils.procurar("Qual o estudante que pretende atribuir ao projeto?", this.c.getListaEstudantes().getEstudantes());
            } while(index == -1);
            estudante = this.c.getListaEstudantes().getEstudantes().get(index);
        }

        do{
            index = Utils.procurar("Qual o orientador que pretende atribuir ao projeto?", this.c.getListaOrientadores().getOrientadores());
        }while(index == -1);
        Orientador orientador = this.c.getListaOrientadores().getOrientadores().get(index);

        String inicio = Utils.lerData("Qual a data de inicio do projeto?");
        String fim = Utils.lerData("Qual a data de fim do projeto?");
        int n_sequencial = this.c.getListaProjetos().sequencial(this.c);
        int estado = estudante != null ? 2 : 1;
        this.c.getListaProjetos().getProjetos().add(new Projeto(titulo, descricao, empresa, orientador, supervisor, estudante,  Data.converterString(inicio), Data.converterString(fim), n_sequencial, estado));
        System.out.println("Projeto inserido com sucesso!\n");
    }

    /**
     * Escolher o que pretendemos gerir do objeto Estudante, Orientador ou Supervisor escolhido
     * @param p Objeto da classe Projeto
     */
    private void gerirProjeto(Projeto p)
    {
        String menu = "==[Gerir Projeto]==\n";
        menu += "1. Ver\n2. Editar\n3. Remover\n0. Regressar";
        int opt = Utils.lerInt(menu, 0, 3, 1, 1);
        if (opt != 0) {
            switch (opt) {
                case 1:
                    this.infoProjeto(p);
                    this.gerirProjeto(p);
                    break;
                case 2:
                    this.editarProjeto(p);
                    this.gerirProjeto(p);
                    break;
                case 3:
                    if(!this.removerProjeto(p))
                        this.gerirProjeto(p);
                    break;
                default:
                    Utils.erroOp();
                    this.gerirProjeto(p);
            }
        }
    }

    /**
     * Obter informações referentes ao objeto escolhido
     * @param p Objeto da classe Projeto
     */
    private void infoProjeto(Projeto p)
    {
        String projeto = "";
        projeto += "==[Informação do projeto selecionado]==\n";
        projeto += "Titulo: " + p.getTitulo() + "]==\n";
        projeto += "Descrição: " + p.getDescricao() + "\n";
        projeto += "Número Sequencial: " + p.getN_sequencial() + "\n";
        projeto += "Estudante Atribuido: " + (p.getEstudante() != null ? p.getEstudante().getNome() : "Não Atribuido") + "\n";
        projeto += "Orientador Responsavel: " + (p.getOrientador() != null ? p.getOrientador().getNome() : "Não Atribuido") + "\n";
        projeto += "Empresa Associada: " + (p.getEmpresa() != null ? p.getEmpresa().getNome() : "Não Atribuida") + "\n";
        projeto += "Supervisor Responsável: " + (p.getSupervisor() != null ? p.getSupervisor().getNome() : "Não Atribuido") + "\n";
        projeto += "Data de inicio: " + p.getInicio() + "\n";
        projeto += "Data de fim: " + p.getFim() + "\n";
        projeto += "Estado: " + p.getEstadoString() + "\n";
        System.out.println(projeto);
    }

    /**
     * Editar os dados pretendidos do objeto escolhido
     * @param p Objeto da classe Projeto
     */
    private void editarProjeto(Projeto p)
    {
        this.infoProjeto(p);
        String menu = "==[Editar]==\n";
        menu += "1. Titulo\n2. Descrição\n3. Estudante\n4. Orientador\n5. Supervisor\n6. Empresa\n7. Classificação\n8. Data de Inicio\n9. Data de Fim\n0. Regressar";
        int editar = Utils.lerInt(menu, 0, 9, 1, 1);
        if (editar != 0){
            switch (editar) {
                case 1:
                    String nome = "";
                    do {
                        String str = Utils.lerLinha("Qual o novo titulo que quer atribuir? Não preencher, caso não queira alterar o valor");
                        if (str != null && str.length() > 0) {
                            if (this.c.getListaProjetos().verificarTitulo(str, p))
                                System.out.println("Já há um projeto registado com esse titulo!\n");
                            else nome = str;
                        } else nome = "none";
                    }while(nome.equals(""));

                    if (!nome.equals("none") && nome.length() > 0 ) {
                        p.setTitulo(nome);
                        System.out.println("O titulo foi editado com sucesso!\n");
                    } else System.out.println("O titulo não foi editado!\n");
                    break;
                case 2:
                    String descricao = Utils.lerLinha("Qual a nova descrição que quer atribuir ao projeto? Não preencher, caso não queira alterar o valor");
                    if (descricao != null && descricao.length() > 0){
                        p.setDescricao(descricao);
                        System.out.println("A descrição do projeto foi editada com sucesso!\n");
                    } else System.out.println("A descrição do projeto não foi editada!\n");
                    break;
                case 3:
                    if(this.c.getListaEstudantes().getEstudantes().size() > 0) {
                        int index;
                        do {
                            String aluno = Utils.lerLinha("Qual o novo estudante que pretende atribuir a este projeto? Não preencher, caso não queira alterar o valor");
                            if (aluno != null && aluno.length() > 0) {
                                index = Utils.procurar(aluno, this.c.getListaEstudantes().getEstudantes());
                                if(index != -1 && this.c.getListaProjetos().verificarEstudante(this.c.getListaEstudantes().getEstudantes().get(index),p)){
                                    index = -1;
                                    System.out.println("O estudante já se encontra noutro projeto!\n");
                                }
                            } else index = -2;
                        } while (index == -1);

                        if (index > -1) {
                            int estudante;
                            if (p.getEstado() == 3 && (estudante = Utils.getIndex(p.getEstudante(), this.c.getListaEstudantes().getEstudantes())) > -1)
                                this.c.getListaEstudantes().getEstudantes().get(estudante).setClassificacao(-1);
                            p.setEstudante(this.c.getListaEstudantes().getEstudantes().get(index));
                            p.setEstado(2);
                            System.out.println("O novo estudante foi atribuido ao projeto com sucesso!\n");
                        } else System.out.println("Não foi atribuido novo estudante ao projeto.\n");
                    } else System.out.println("Não há estudantes registados.\n");
                    break;
                case 4:
                    if(this.c.getListaOrientadores().getOrientadores().size() > 0) {
                        int index;
                        do {
                            String orientador = Utils.lerLinha("Qual o novo orientador que pretende atribuir a este projeto? Não preencher, caso não queira alterar o valor");
                            if (orientador != null && orientador.length() > 0) {
                                index = Utils.procurar(orientador, this.c.getListaOrientadores().getOrientadores());
                            } else index = -2;
                        } while (index == -1);

                        if (index > -1) {
                            p.setOrientador(this.c.getListaOrientadores().getOrientadores().get(index));
                            System.out.println("O novo orientador foi atribuido ao projeto com sucesso!\n");
                        } else System.out.println("Não foi atribuido novo orientador ao projeto.\n");
                    } else System.out.println("Não há orientadores registados.\n");
                    break;
                case 5:
                    if(p.getEmpresa() != null) {
                        if (this.c.getListaSupervisores().getSupervisores().size() > 0) {
                            int index;
                            do {
                                String supervisor = Utils.lerLinha("Qual o novo supervisor que pretende atribuir a este projeto? Não preencher, caso não queira alterar o valor");
                                if (supervisor != null && supervisor.length() > 0) {
                                    index = Utils.procurar(supervisor, this.c.getListaSupervisores().getSupervisores());
                                    if(index > -1){
                                        if(!this.c.getListaSupervisores().getSupervisores().get(index).getEmpresa().equals(p.getEmpresa())){
                                            index = -1;
                                            System.out.println("O supervisor não pertence à empresa do projeto selecionado!\n");
                                        }
                                    }
                                } else index = -2;
                            } while (index == -1);

                            if (index > -1) {
                                p.setSupervisor(this.c.getListaSupervisores().getSupervisores().get(index));
                                System.out.println("O novo supervisor foi atribuido ao projeto com sucesso!\n");
                            } else System.out.println("Não foi atribuido novo supervisor ao projeto.\n");
                        } else System.out.println("Não há supervisores registados.\n");
                    } else System.out.println("Não há uma empresa atribuida ao projeto, logo não pode atribuir supervisor.\n");
                    break;
                case 6:
                        if (this.c.getListaEmpresas().getEmpresas().size() > 0) {
                            int index;
                            do {
                                String empresa = Utils.lerLinha("Qual a nova empresa que pretende atribuir a este projeto? Não preencher, caso não queira alterar o valor.");
                                if (empresa != null && empresa.length() > 0) {
                                    index = Utils.procurar(empresa, this.c.getListaEmpresas().getEmpresas());
                                } else index = -2;
                            } while (index == -1);

                            if (index > -1) {
                                p.setEmpresa(this.c.getListaEmpresas().getEmpresas().get(index));
                                p.setSupervisor(null);
                                System.out.println("A nova empresa foi atribuida ao projeto com sucesso!\n");
                            } else System.out.println("Não foi atribuida nova empresa ao projeto.\n");
                        } else System.out.println("Não há empresas registadas.\n");
                    break;
                case 7:
                    int nota_maxima = 20;
                    int nota = Utils.lerInt("Qual a nova classificação que quer atribuir ao projeto [0-" + nota_maxima + "]? Atribuir -1, caso não queira alterar o valor", 0,nota_maxima, 1, 2);
                    if (nota > -1 && nota <= nota_maxima){
                        int estudante = Utils.getIndex(p.getEstudante(), this.c.getListaEstudantes().getEstudantes());
                        this.c.getListaEstudantes().getEstudantes().get(estudante).setClassificacao(nota);
                        p.setEstado(3);
                        System.out.println("A classificação do projeto foi editada com sucesso!\n");
                    }
                    else if(nota > nota_maxima) System.out.println("A classificação não foi atribuida por ser maior do que o máximo permitido (" + nota_maxima + " valores)\n");
                    else System.out.println("A classificação do projeto não foi editada!\n");
                    break;
                case 8:
                    String data_inicio = "";
                    do {
                        String data = Utils.lerLinha("Qual a nova data de inicio que quer atribuir? Não preencher, caso não queira alterar o valor");
                        if (data != null && data.length() > 0){
                            if(Data.verificarData(data) && Data.converterString(data).isMenorOrEq(p.getFim()))
                                data_inicio = data;
                            else System.out.println("A data não é valida!\n");
                        }else data_inicio = "none";
                    } while(data_inicio.equals(""));

                    if(!data_inicio.equals("")){
                        p.setInicio(Data.converterString(data_inicio));
                        System.out.println("A data de inicio do projeto foi editada com sucesso!");
                    } else System.out.println("A data de inicio do projeto não foi editada!");
                    break;
                case 9:
                    String data_fim = "";
                    do {
                        String data = Utils.lerLinha("Qual a nova data de inicio que quer atribuir? Não preencher, caso não queira alterar o valor");
                        if (data != null && data.length() > 0){
                            if(Data.verificarData(data) && Data.converterString(data).isMaiorOrEq(p.getInicio()))
                                data_fim = data;
                            else System.out.println("A data não é valida!\n");
                        }else data_fim = "none";
                    } while(data_fim.equals(""));

                    if(!data_fim.equals("")){
                        p.setInicio(Data.converterString(data_fim));
                        System.out.println("A data de fim do projeto foi editada com sucesso!");
                    } else System.out.println("A data de fim do projeto não foi editada!");
                    break;
                default:
                    Utils.erroOp();
            }
            this.editarProjeto(p);
        }
    }

    /**
     * Remover Projeto escolhido da lista
     * @param p Objeto da classe Projeto
     */
    private boolean removerProjeto(Projeto p)
    {
        if(Utils.confirma("\nPretende remover o projeto escolhido? S - Sim | N - Não"))
        {
            this.c.getListaProjetos().removerProjeto(p, this.c);
            System.out.println("A empresa foi eliminada com sucesso!");
            return true;
        }
        else {
            System.out.println("A empresa não foi eliminada.");
            return false;
        }
    }

    /*
    *
    * Métodos Extra
    *
    * */

    /**
     * Projetos orientador por um docente
     */
    private void projetosOrientador()
    {
        int index;
        do{
            String str = Utils.lerLinha("Qual o orientador que pretende ver? Não preencher, caso pretenda cancelar a ação");
            if(str != null && str.length() > 0){
                index = Utils.procurar(str, this.c.getListaOrientadores().getOrientadores());
            } else index = -2;
        }while(index == -1);

        if(index > -1){
            Orientador o = this.c.getListaOrientadores().getOrientadores().get(index);
            System.out.println("\n==[Projetos orientados por " + o.getNome() + "]==");
            System.out.println(this.c.getListaProjetos().getOrientadorProjetos(o));
        } else System.out.println("Ação cancelada!");
    }

    /**
     * Empresa com mais projetos
     */
    private void empresaMaisProjetos()
    {
        if(this.c.getListaProjetos().getProjetos().size() > 0){
            System.out.println("\nEmpresa que oferece mais projetos: " + this.c.getListaProjetos().getEmpresaMaisProjetos(this.c.getListaEmpresas().getEmpresas()) + ".");
        } else System.out.println("Não há projetos suficientes para fazer o cálculo!");
    }

    /**
     * Estudantes com nota igual ou superior à pedida
     */
    private void estudanteNotaMaisAlta(){
        if(this.c.getListaEstudantes().getEstudantes().size() > 1) {
            int nota = Utils.lerInt("Qual o valor da nota? Escolher -1, caso pretenda cancelar a ação", 0, 20, 1, 2);
            if (nota > -1) {
                System.out.println("\n==[Alunos com nota igual ou superior a " + nota + "]==");
                System.out.println(this.c.getListaEstudantes().getNotaMaisAlta(nota));
            }
            else
                System.out.println("Ação cancelada!");
        } else System.out.println("Não há estudantes suficientes para fazer o cálculo!");
    }
}
