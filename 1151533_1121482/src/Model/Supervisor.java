package Model;

import Model.utils.*;
import java.io.Serializable;

public class Supervisor extends Pessoa implements Serializable
{
    private Empresa empresa;

    public Supervisor(String nome, Data nascimento, int cc, int nif,String email, Empresa empresa)
    {
        super(nome, nascimento, cc, nif, email);
        this.empresa = empresa;
    }

    public Empresa getEmpresa()
    {
        return this.empresa;
    }

    public void setEmpresa(Empresa empresa)
    {
        this.empresa = empresa;
    }

    @Override
    public String toString()
    {
        return this.getNome();
    }
}
