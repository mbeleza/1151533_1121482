package Model.listas;

import Model.Empresa;
import Model.Supervisor;
import java.io.Serializable;
import java.util.ArrayList;

public class ListaSupervisores implements Serializable
{
    private ArrayList<Supervisor> listaSupervisores;

    public ListaSupervisores()
    {
        listaSupervisores = new ArrayList<>();
    }

    public ArrayList<Supervisor> getSupervisores()
    {
        return listaSupervisores;
    }

    /**
     * Obter Supervisores de uma Empresa
     * @param e Empresa a procurar
     * @return ArrayList com Supervisores de "e"
     */
    public ArrayList<Supervisor> getEmpresaSupervisores(Empresa e)
    {
        ArrayList<Supervisor> lista = new ArrayList<>();
        for(Supervisor s : this.listaSupervisores){
            if(s.getEmpresa().equals(e))
                lista.add(s);
        }
        return lista;
    }

    /**
     * Atualizar a Empresa em todos os Supervisores em que a mesma esteja associada
     * @param before Objecto da classe Empresa antes de ser editada
     * @param after Objecto da classe Empresa depois de ser editada
     */
    public void editarEmpresaSupervisores(Empresa before, Empresa after)
    {
        for (Supervisor s : this.listaSupervisores)
            if (s.getEmpresa() != null)
                if (s.getEmpresa().equals(before))
                    s.setEmpresa(after);
    }

    /**
     * Verificar se existe algum supervisor com o nome "strNome"
     * @param strNome Nome a ser verificado
     * @param est Supervisor a não verificar, caso esteja a editar "est"
     * @return True se existir alguma supervisor com o nome pedido, excluindo "est"
     */
    public boolean verificarNome(String strNome, Supervisor est){
        for(Supervisor e : this.listaSupervisores){
            if(e.getNome().equalsIgnoreCase(strNome) && !e.equals(est))
                return true;
        }
        return false;
    }

    /**
     * Verificar se existe alguma supervisor com o cc "strCc"
     * @param intCc CC a ser verificado
     * @param est Supervisor a não verificar, caso esteja a editar "est"
     * @return True se existir alguma Supervisor com o cc pedido, excluindo "est"
     */
    public boolean verificarCc(int intCc, Supervisor est){
        for(Supervisor e : this.listaSupervisores){
            if(e.getCc() == intCc && !e.equals(est))
                return true;
        }
        return false;
    }

    /**
     * Verificar se existe alguma supervisor com o nif "strCc"
     * @param intNif Nif a ser verificado
     * @param est Supervisor a não verificar, caso esteja a editar "est"
     * @return True se existir alguma Supervisor com o nif pedido, excluindo "est"
     */
    public boolean verificarNif(int intNif, Supervisor est){
        for(Supervisor e : this.listaSupervisores){
            if(e.getNif() == intNif && !e.equals(est))
                return true;
        }
        return false;
    }

    /**
     * Verificar se existe alguma supervisor com o email "strEmail"
     * @param strEmail Email a ser verificado
     * @param est Supervisor a não verificar, caso esteja a editar "est"
     * @return True se existir alguma Supervisor com o email pedido, excluindo "est"
     */
    public boolean verificarEmail(String strEmail, Supervisor est){
        for(Supervisor e : this.listaSupervisores){
            if(e.getEmail().equals(strEmail) && !e.equals(est))
                return true;
        }
        return false;
    }

    /**
     * Remover Supervisores associados a uma Empresa
     * @param e Empresa escolhida
     */
    public void removerEmpresaSupervisores(Empresa e)
    {
        int index = -1;
        for(Supervisor s : this.listaSupervisores){
            index++;
            if(s.getEmpresa().equals(e))
                this.listaSupervisores.remove(index);
        }
    }

    /**
     * Remover Supervisor da lista
     * @param s Supervisor a remover
     */
    public void removerSupervisor(Supervisor s)
    {
        int index = -1;
        for(Supervisor sup : this.listaSupervisores){
            index++;
            if(sup.equals(s))
                this.listaSupervisores.remove(index);
        }
    }
}
