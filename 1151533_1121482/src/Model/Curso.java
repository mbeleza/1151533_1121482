package Model;

import Model.listas.*;
import java.io.Serializable;

public class Curso implements Serializable
{
    private int anoLectivo;
    private ListaEmpresas lemp;
    private ListaEstudantes lest;
    private ListaOrientadores lo;
    private ListaProjetos lp;
    private ListaSupervisores ls;

    public Curso()
    {
        this.anoLectivo = 0;
        this.lemp = new ListaEmpresas();
        this.lest = new ListaEstudantes();
        this.lo = new ListaOrientadores();
        this.lp = new ListaProjetos();
        this.ls = new ListaSupervisores();
    }

    public int getAnoLectivo()
    {
        return this.anoLectivo;
    }

    public void setAnoLectivo(int anoLectivo)
    {
        this.anoLectivo = anoLectivo;
    }

    public ListaEmpresas getListaEmpresas()
    {
        return lemp;
    }

    public void setListaEmpresas(ListaEmpresas le)
    {
        this.lemp = le;
    }

    public ListaEstudantes getListaEstudantes()
    {
        return lest;
    }

    public void setListaEstudantes(ListaEstudantes lest)
    {
        this.lest = lest;
    }

    public ListaOrientadores getListaOrientadores()
    {
        return lo;
    }

    public void setListaOrientadores(ListaOrientadores lo)
    {
        this.lo = lo;
    }

    public ListaProjetos getListaProjetos()
    {
        return lp;
    }

    public void setListaProjetos(ListaProjetos lp)
    {
        this.lp = lp;
    }

    public ListaSupervisores getListaSupervisores()
    {
        return ls;
    }

    public void setListaSupervisores(ListaSupervisores ls)
    {
        this.ls = ls;
    }
}
