package Model.listas;

import Model.*;
import Model.utils.Utils;
import java.io.Serializable;
import java.util.ArrayList;

public class ListaProjetos implements Serializable
{
    private ArrayList<Projeto> listaProjetos;

    public ListaProjetos()
    {
        this.listaProjetos = new ArrayList<>();
    }

    public ArrayList<Projeto> getProjetos()
    {
        return this.listaProjetos;
    }

    /**
     * Obter número sequencial de Projeto
     * @param c Objeto do Curso para obter ano lectivo correspondente
     * @return número sequencial
     */
    public int sequencial(Curso c)
    {
        if(this.getProjetos().size() > 0)
            return this.listaProjetos.get(getProjetos().size()-1).getN_sequencial() + 1;
        else
            return (int) (c.getAnoLectivo()*Math.pow(10,4)+1);
    }

    /**
     * Obter Projetos de um Orientador
     * @param o Orientador escolhido
     */
    public String getOrientadorProjetos(Orientador o)
    {
        int index = 0;
        String lista = "";
        for(Projeto p : this.listaProjetos)
            if (p.getOrientador() != null && p.getOrientador().equals(o)) {
                index++;
                lista += index + ". " + p.getTitulo() + "\n";
            }

        if(index > 0)
            return lista;
        else
            return "Não há nenhum projeto orientado por este docente.";
    }

    /**
     * Obter primeira empresa que tiver mais projetos
     * @param emp Lista de empresas
     */
    public String getEmpresaMaisProjetos(ArrayList<Empresa> emp)
    {
        int index = 0;
        int max = 0;
        Empresa empresa = null;
        for(Empresa e : emp) {
            for (Projeto p : this.listaProjetos)
                if (p.getEmpresa() != null && p.getEmpresa().equals(e))
                    index++;

            if(index > max) {
                empresa = e;
                max = index;
            }
        }

        if(empresa != null)
            return empresa.getNome();
        else
            return "Não encontrada";
    }

    /**
     * Atualizar o Estudante, Orientador ou Supervisor em todos os Projetos em que os mesmos estejam associados
     * @param before Objecto da classe Estudante, Orientador ou Supervisor antes de ser editado
     * @param after Objecto da classe Estudante, Orientador ou Supervisor depois de ser editado
     */
    public void editarPessoaProjetos(Object before, Object after)
    {
        if(before instanceof Estudante) {
            for (Projeto p : this.listaProjetos)
                if (p.getEstudante() != null && p.getEstudante().equals(before))
                    p.setEstudante((Estudante) after);
        }
        else if(before instanceof Orientador) {
            for (Projeto p : this.listaProjetos)
                if (p.getOrientador() != null && p.getOrientador().equals(before))
                    p.setOrientador((Orientador) after);
        }
        else if(before instanceof Supervisor) {
            for (Projeto p : this.listaProjetos)
                if (p.getSupervisor() != null && p.getSupervisor().equals(before))
                    p.setSupervisor((Supervisor) after);
        }
    }

    /**
     * Atualizar a Empresa em todos os Projetos em que a mesma esteja associada
     * @param before Objecto da classe Empresa antes de ser editada
     * @param after Objecto da classe Empresa depois de ser editada
     */
    public void editarEmpresaProjetos(Empresa before, Empresa after)
    {
        for (Projeto p : this.listaProjetos)
            if (p.getEmpresa() != null && p.getEmpresa().equals(before))
                p.setEmpresa(after);
    }

    /**
     * Verificar se existe algum projeto com o titulo "strTitulo"
     * @param strTitulo Titulo a ser verificado
     * @param p2 Projeto a não verificar, caso esteja a editar "p2"
     * @return True se existir alguma projeto com o nome pedido, excluindo "p2"
     */
    public boolean verificarTitulo(String strTitulo, Projeto p2)
    {
        for(Projeto p : this.listaProjetos){
            if(p.getTitulo().equalsIgnoreCase(strTitulo) && !p.equals(p2))
                return true;
        }
        return false;
    }

    /**
     * Verificar se existe algum projeto com o estudante "strEst"
     * @param strEst Estudante a ser verificado
     * @param p2 Projeto a não verificar, caso esteja a editar "p2"
     * @return True se existir alguma projeto com o estudante pedido, excluindo "p2"
     */
    public boolean verificarEstudante(Estudante strEst, Projeto p2)
    {
        for(Projeto p : this.listaProjetos){
            if(p.getEstudante() != null && p.getEstudante().equals(strEst) && !p.equals(p2))
                return true;
        }
        return false;
    }

    /**
     * Remover uma empresa e respetivos supervisores dos projetos associados
     * @param e Empresa a remover
     */
    public void removerEmpresaProjetos(Empresa e)
    {
        for(Projeto p : this.listaProjetos){
            if(p.getEmpresa() != null && p.getEmpresa().equals(e)){
                p.setEmpresa(null);
                p.setSupervisor(null);
            }
        }
    }

    /**
     * Remover um Estudante, Orientador ou Supervisor e respetivos supervisores dos projetos associados
     * @param o Estudante, Orientador ou Supervisor a remover
     */
    public void removerPessoaProjetos(Object o)
    {
        for(Projeto p : this.listaProjetos){
            if(o instanceof Estudante){
                if(p.getEstudante() != null && p.getEstudante().equals(o)) {
                    p.setEstudante(null);
                    p.setEstado(1);
                }
            } else if(o instanceof Orientador){
                if(p.getOrientador() != null &&  p.getOrientador().equals(o))
                    p.setOrientador(null);
            } else if(o instanceof Supervisor){
                if(p.getSupervisor() != null &&  p.getSupervisor().equals(o))
                    p.setSupervisor(null);
            }
        }
    }

    /**
     * Remover Projeto da lista
     * @param p Projeto a remover
     */
    public void removerProjeto(Projeto p, Curso c)
    {
        int index = -1;
        for(Projeto pro : this.listaProjetos){
            index++;
            if(pro.equals(p)) {
                c.getListaEstudantes().getEstudantes().get(Utils.getIndex(p.getEstudante(), c.getListaEstudantes().getEstudantes())).setClassificacao(-1);
                this.listaProjetos.remove(index);
            }
        }
    }
}
