package Model;

import Model.utils.*;
import java.io.Serializable;

public class Orientador extends Pessoa implements Serializable
{
    private String sigla;

    public Orientador(String nome, Data nascimento, int cc, int nif, String email, String sigla)
    {
        super(nome, nascimento, cc, nif, email);
        this.sigla = sigla;
    }

    public String getSigla()
    {
        return sigla;
    }

    public void setSigla(String sigla)
    {
        this.sigla = sigla;
    }

    @Override
    public String toString()
    {
        return this.getNome();
    }
}
