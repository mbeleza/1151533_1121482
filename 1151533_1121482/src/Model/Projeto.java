package Model;

import Model.utils.*;
import java.io.Serializable;

public class Projeto implements Serializable
{
    
    private String titulo;
    private String descricao;
    private Empresa empresa;
    private Orientador orientador;
    private Supervisor supervisor;
    private Estudante estudante;
    private Data data_inicio;
    private Data data_fim;
    private int n_sequencial;
    private int estado;                 // 1. Disponivel | 2. Atribuido | 3. Concluido

    public Projeto(String titulo, String descricao, Empresa empresa, Orientador orientador, Supervisor supervisor, Estudante estudante, Data data_inicio, Data data_fim, int n_sequencial, int estado)
    {
        this.titulo = titulo;
        this.descricao = descricao;
        this.empresa = empresa;
        this.orientador = orientador;
        this.supervisor = supervisor;
        this.data_inicio = data_inicio;
        this.data_fim = data_fim;
        this.estudante = estudante;
        this.n_sequencial = n_sequencial;
        this.estado = estado;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public Data getInicio()
    {
        return this.data_inicio;
    }

    public void setInicio(Data data_inicio)
    {
        this.data_inicio = data_inicio;
    }

    public Data getFim()
    {
        return this.data_fim;
    }

    public void setFim(Data data_fim)
    {
        this.data_fim = data_fim;
    }

    public int getN_sequencial()
    {
        return n_sequencial;
    }

    public int getEstado()
    {
        return estado;
    }

    public String getEstadoString()
    {
        String estado = "";
        switch(this.estado)
        {
            case 1: estado = "Disponivel"; break;
            case 2: estado = "Atribuido"; break;
            case 3: estado = "Concluido"; break;
        }
        return estado;
    }

    public void setEstado(int estado)
    {
        this.estado = estado;
    }

    public Empresa getEmpresa()
    {
        return this.empresa;
    }

    public void setEmpresa(Empresa empresa)
    {
        this.empresa = empresa;
    }

    public Orientador getOrientador()
    {
        return this.orientador;
    }

    public void setOrientador(Orientador orientador)
    {
        this.orientador = orientador;
    }

    public Supervisor getSupervisor()
    {
        return this.supervisor;
    }

    public void setSupervisor(Supervisor supervisor)
    {
        this.supervisor = supervisor;
    }

    public Estudante getEstudante()
    {
        return this.estudante;
    }

    public void setEstudante(Estudante estudante)
    {
        this.estudante = estudante;
    }
    
    @Override
    public String toString()
    {
        return this.titulo + " (#" + this.n_sequencial + ")";
    }
}
