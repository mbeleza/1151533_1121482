package Model;

import Model.utils.*;
import java.io.Serializable;

public class Pessoa implements Serializable
{
    
    private String nome;
    private Data nascimento;
    private int cc;
    private int nif;
    private String email;

    public Pessoa(String nome, Data nascimento, int cc, int nif, String email)
    {
        this.nome = nome;
        this.nascimento = nascimento;
        this.cc = cc;
        this.nif = nif;
        this.email = email;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public Data getNascimento()
    {
        return nascimento;
    }

    public void setNascimento(Data nascimento)
    {
        this.nascimento = nascimento;
    }

    public int getCc()
    {
        return cc;
    }

    public void setCc(int cc)
    {
        this.cc = cc;
    }

    public int getNif()
    {
        return nif;
    }

    public void setNif(int nif)
    {
        this.nif = nif;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    @Override
    public String toString()
    {
        return this.getNome();
    }
}
