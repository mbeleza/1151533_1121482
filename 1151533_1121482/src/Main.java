import UI.Menu;
import Model.*;
import Model.utils.*;
import java.io.IOException;
import java.io.Serializable;

public class Main implements Serializable
{
    public static void main(String[] args)
    {
        Curso c;
        Menu m;

        System.out.println("==[Aplicação Iniciada]==");
        c = Ficheiro.lerInformacaoCurso();

        if(c.getAnoLectivo() == 0){
            int ano = Utils.lerInt("\nEm que ano em que se iniciam os projetos? [1980-2100]", 1980, 2100, 4,4);
            c.setAnoLectivo(ano);
            if(Utils.confirma("\nPretende inserir dados de demonstração? S - Sim | N - Não")){
                for(int i = 1; i < 3; i++) {
                    c.getListaEmpresas().getEmpresas().add(new Empresa("Empresa"+i, "Rua da Empresa"+i, 512345678+i, 221234567+i));
                    c.getListaEstudantes().getEstudantes().add(new Estudante("Estudante"+i, Data.converterString("23/7/197"+i),
                            12345678+i, 123456789+i, "1234567" + i + "@isep.ipp.pt", "Rua do Estudante"+i, 1234567+i, -1));
                    c.getListaOrientadores().getOrientadores().add(new Orientador("Professor"+i, Data.converterString("13/5/193"+i),
                            12345678+i, 223456789+i, "prof" + i + "@isep.ipp.pt", "PROF"+i));
                    c.getListaSupervisores().getSupervisores().add(new Supervisor("Supervisor"+i, Data.converterString("12/11/193"+i),
                            12345678+i, 123456789+i, "super" + i + "@empresa" + i + ".pt", c.getListaEmpresas().getEmpresas().get(i-1)));
                    c.getListaProjetos().getProjetos().add(new Projeto(("Projeto"+i), ("Descrição do Projeto"+i), null, c.getListaOrientadores().getOrientadores().get(i-1),
                            null, null, Data.converterString("13/5/193"+i), Data.converterString("13/8/193"+i), c.getListaProjetos().sequencial(c), 1));
                }
                System.out.println("\nDados de demonstração inseridos com sucesso!");
            }
        }

        m = new Menu(c);
        boolean flag;

        do
        {
            flag = m.listarMenu();
        }while(flag);

        try
        {
            Ficheiro.guardaInformacaoCurso(c);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        System.out.println("\n==[Aplicação Terminada]==");
    }

}
