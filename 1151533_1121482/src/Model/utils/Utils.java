package Model.utils;

import Model.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Utils
{

    public static String lerLinha(String strPrompt)
    {
        Scanner in = new Scanner(System.in);
        try
        {
            System.out.println(strPrompt);
            return in.nextLine();
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean confirma(String sMessage)
    {
        String strConfirma;
        do {
            strConfirma = lerLinha(sMessage);
        } while (!strConfirma.equalsIgnoreCase("s") && !strConfirma.equalsIgnoreCase("n"));

        return strConfirma.equalsIgnoreCase("s");
    }

    public static Object apresentaESeleciona(ArrayList list,String sHeader)
    {
        apresentaLista(list,sHeader);
        System.out.println("0. Regressar");
        return selecionaObject(list);
    }

    public static void apresentaLista(ArrayList list,String sHeader)
    {
        System.out.println(sHeader);
        int index = 0;
        for (Object o : list)
        {
            index++;
            System.out.println(index + ". " + o.toString());
        }
    }

    private static Object selecionaObject(ArrayList list)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.lerLinha("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > list.size());

        if (nOpcao == 0)
        {
            return null;
        } else
        {
            return list.get(nOpcao - 1);
        }
    }

    public static int lerInt(String strPrompt, int n_min, int n_max, int lim_min, int lim_max)
    {
        do
        {
            try
            {
                String strInt = lerLinha(strPrompt);
                int iInt = Integer.parseInt(strInt);
                if(iInt != -1) {
                    if (strInt.length() < lim_min || strInt.length() > lim_max || iInt < n_min || iInt > n_max)
                        throw new NumberFormatException();
                }
                return iInt;
            }
            catch (NumberFormatException ex)
            {
                System.out.println("Valor inválido!");
            }
        } while (true);
    }

    public static double lerDouble(String strPrompt)
    {
        do
        {
            try
            {
                String strDbl = lerLinha(strPrompt);
                double dValor = Double.parseDouble(strDbl);
                return dValor;
            }
            catch (NumberFormatException ex)
            {
                System.out.println("Valor inválido!");
            }
        } while (true);
    }

    public static String lerEmail(String strPrompt)
    {
        do
        {
            try {
                String email = lerLinha(strPrompt);
                if (email != null && email.length() > 0)
                    if (email.trim().isEmpty() || !(email.matches("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9.]+\\.[a-zA-Z]{2,3}")))
                        throw new IOException();
                return email;

            } catch (IOException ex)
            {
                System.out.println("Email inválido!");
            }
        } while (true);
    }

    public static String lerData(String strPrompt)
    {
        do
        {
            try
            {
                String strDbl = lerLinha(strPrompt);
                return strDbl;
            } catch (NumberFormatException ex)
            {
                System.out.println("Data inválida!");
            }
        } while (true);
    }

    public static void erroOp()
    {
        System.out.println("Valor inválido!");
    }

    public static int getIndex(Object a, ArrayList list)
    {
        int index = -1;
        for(Object b : list) {
            index++;
            if (a.equals(b))
                return index;
        }
        return -1;
    }

    public static int procurar(String str, ArrayList list)
    {
        int index = -1;
        if (str != null && str.length() >= 0 ) {
            for (Object o : list) {
                index++;
                if (o instanceof Empresa) {
                    if (((Empresa) o).getNome().equalsIgnoreCase(str))
                        return index;
                } else if (o instanceof Estudante) {
                    if (((Estudante) o).getNome().equalsIgnoreCase(str))
                        return index;
                } else if (o instanceof Orientador) {
                    if (((Orientador) o).getNome().equalsIgnoreCase(str))
                        return index;
                } else if (o instanceof Supervisor) {
                    if (((Supervisor) o).getNome().equalsIgnoreCase(str))
                        return index;
                } else if (o instanceof Projeto) {
                    if (((Projeto) o).getTitulo().equalsIgnoreCase(str))
                        return index;
                }
            }
            System.out.println("Não encontramos nenhum dado com esse nome!");
        }
        return -1;

    }

}

