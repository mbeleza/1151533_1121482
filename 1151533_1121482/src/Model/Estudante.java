package Model;

import Model.utils.*;
import java.io.Serializable;

public class Estudante extends Pessoa implements Serializable
{
    private String morada;
    private int mecanografico;
    private int classificacao = -1;
    
    public Estudante(String nome, Data nascimento, int cc, int nif, String email, String morada, int mecanografico, int classificacao)
    {
        super(nome, nascimento, cc, nif, email);
        this.morada = morada;
        this.mecanografico = mecanografico;
        this.classificacao = classificacao;
    }

    public String getMorada()
    {
        return morada;
    }

    public void setMorada(String morada)
    {
        this.morada = morada;
    }

    public int getMecanografico()
    {
        return mecanografico;
    }

    public void setMecanografico(int mecanografico)
    {
        this.mecanografico = mecanografico;
    }

    public int getClassificacao()
    {
        return this.classificacao;
    }

    public void setClassificacao(int classificacao)
    {
        this.classificacao = classificacao;
    }

    @Override
    public String toString()
    {
        return this.getNome();
    }
}
