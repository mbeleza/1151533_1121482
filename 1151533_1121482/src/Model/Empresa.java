package Model;

import java.io.Serializable;

public class Empresa implements Serializable
{
    private String nome;
    private String morada;
    private int nipc;
    private int telefone;

    public Empresa(String nome, String morada, int nipc, int telefone)
    {
        this.nome = nome;
        this.morada = morada;
        this.nipc = nipc;
        this.telefone = telefone;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getMorada()
    {
        return morada;
    }

    public void setMorada(String morada)
    {
        this.morada = morada;
    }

    public int getNipc()
    {
        return nipc;
    }

    public void setNipc(int nipc)
    {
        this.nipc = nipc;
    }

    public int getTelefone()
    {
        return telefone;
    }

    public void setTelefone(int telefone)
    {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return this.nome;
    }


}
