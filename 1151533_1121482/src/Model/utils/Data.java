package Model.utils;

import java.io.Serializable;

public class Data implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * O ano da data.
     */
    private int ano;

    /**
     * O mês da data.
     */
    private int mes;

    /**
     * O dia da data.
     */
    private int dia;

    /**
     * O ano da data por omissão.
     */
    private static final int ANO_POR_OMISSAO = 1;

    /**
     * O mês da data por omissão.
     */
    private static final int MES_POR_OMISSAO = 1;

    /**
     * O dia da data por omissão.
     */
    private static final int DIA_POR_OMISSAO = 1;

    /**
     * Nomes dos dias da semana.
     */
    private static String[] nomeDiaDaSemana = {"Domingo", "Segunda-feira",
            "Terça-feira", "Quarta-feira",
            "Quinta-feira", "Sexta-feira",
            "Sábado"};

    /**
     * Número de dias de cada mês do ano.
     */
    private static int[] diasPorMes = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30,
            31, 30, 31};

    /**
     * Nomes dos meses do ano.
     */
    private static String[] nomeMes = {"Inválido", "Janeiro", "Fevereiro",
            "Março", "Abril", "Maio", "Junho",
            "Julho", "Agosto", "Setembro",
            "Outubro", "Novembro", "Dezembro"};

    /**
     * Constrói uma instância de Data recebendo o ano, o mês e o dia.
     *
     * @param dia o dia da data
     * @param mes o mês da data
     * @param ano o ano da data
     */
    public Data(int dia, int mes, int ano)
    {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }

    /**
     * Constrói uma instância de Data com a data por omissão.
     */
    public Data()
    {
        dia = DIA_POR_OMISSAO;
        mes = MES_POR_OMISSAO;
        ano = ANO_POR_OMISSAO;
    }

    /**
     * Devolve o ano da data.
     *
     * @return ano da data
     */
    public int getAno()
    {
        return ano;
    }

    /**
     * Devolve o mês da data.
     *
     * @return mês da data
     */
    public int getMes()
    {
        return mes;
    }

    /**
     * Devolve o dia da data.
     *
     * @return dia da data
     */
    public int getDia()
    {
        return dia;
    }

    /**
     * Modifica o ano, o mês e o dia da data.
     *
     * @param ano o novo ano da data
     * @param mes o novo mês da data
     * @param dia o novo dia da data
     */
    public void setData(int dia, int mes, int ano)
    {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }

    /**
     * Objeto Data para String
     *
     * @return Data em String com dd
     */
    public String toString()
    {
        return this.dia + "/" + this.mes + "/" + this.ano;
    }

    /**
     * Devolve a data no formato:%04d/%02d/%02d.
     *
     * @return caraterísticas da data
     */
    public String toAnoMesDiaString()
    {
        return String.format("%02d/%02d/%04d", this.dia, this.mes, this.ano);
    }

    /**
     * Devolve o dia da semana da data.
     *
     * @return dia da semana da data
     */
    public String diaDaSemana()
    {
        int totalDias = contaDias();
        totalDias = totalDias % 7;

        return nomeDiaDaSemana[totalDias];
    }

    /**
     * Devolve true se a data for maior do que a data recebida por parâmetro.
     * Se a data for menor ou igual à data recebida por parâmetro, devolve
     * false.
     *
     * @param outraData a outra data com a qual se compara a data
     * @return true se a data for maior do que a data recebida por parâmetro,
     * caso contrário devolve false
     */
    public boolean isMaior(Data outraData)
    {
        int totalDias = contaDias();
        int totalDias1 = outraData.contaDias();

        return totalDias > totalDias1;
    }

    public boolean isMaiorOrEq(Data outraData)
    {
        int totalDias = contaDias();
        int totalDias1 = outraData.contaDias();

        return totalDias >= totalDias1;
    }

    public boolean isMenor(Data outraData)
    {
        int totalDias = contaDias();
        int totalDias1 = outraData.contaDias();

        return totalDias < totalDias1;
    }

    public boolean isMenorOrEq(Data outraData)
    {
        int totalDias = contaDias();
        int totalDias1 = outraData.contaDias();

        return totalDias <= totalDias1;
    }

    /**
     * Devolve a diferença em número de dias entre a data e a data recebida por
     * parâmetro.
     *
     * @param outraData a outra data com a qual se compara a data para calcular
     *                  a diferença do número de dias
     * @return diferença em número de dias entre a data e a data recebida por
     * parâmetro
     */
    public int diferenca(Data outraData)
    {
        int totalDias = contaDias();
        int totalDias1 = outraData.contaDias();

        return Math.abs(totalDias - totalDias1);
    }

    /**
     * Devolve a diferença em número de dias entre a data e a data recebida por
     * parâmetro com ano, mês e dia
     *
     * @param ano o ano da data com a qual se compara a data para calcular a
     *            diferença do número de dias
     * @param mes o mês da data com a qual se compara a data para calcular a
     *            diferença do número de dias
     * @param dia o dia da data com a qual se compara a data para calcular a
     *            diferença do número de dias
     * @return diferença em número de dias entre a data e a data recebida por
     * parâmetro com ano, mês e dia
     */
    public int diferenca(int ano, int mes, int dia)
    {
        int totalDias = contaDias();
        Data outraData = new Data(ano, mes, dia);
        int totalDias1 = outraData.contaDias();

        return Math.abs(totalDias - totalDias1);
    }

    /**
     * Devolve true se o ano passado por parâmetro for bissexto.
     * Se o ano passado por parâmetro não for bissexto, devolve false.
     *
     * @param ano o ano a validar
     * @return true se o ano passado por parâmetro for bissexto, caso contrário
     * devolve false
     */
    public static boolean isAnoBissexto(int ano)
    {
        return ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0;
    }

    /**
     * Devolve o número de dias desde o dia 1/1/1 até à data.
     *
     * @return número de dias desde o dia 1/1/1 até à data
     */
    private int contaDias()
    {
        int totalDias = 0;

        for (int i = 1; i < this.ano; i++) {
            totalDias += isAnoBissexto(i) ? 366 : 365;
        }
        for (int i = 1; i < this.mes; i++) {
            totalDias += diasPorMes[i];
        }
        totalDias += (isAnoBissexto(this.ano) && this.mes > 2) ? 1 : 0;
        totalDias += this.dia;

        return totalDias;
    }

    /**
     * Verificar se @param se encontra no formato pretendido (dd-mm-aaaa ou dd/mm/aaaa)
     *
     * @param strData String data a verificar
     * @return True se estiver no formato pretendido
     */
    private static boolean verificarFormatoData(String strData)
    {
        return strData.matches("^([0-9]{1,2}[-\\/]){2}[0-9]{4}$");
    }

    /**
     * Converte uma data no formato dd-mm-aaaa ou dd/mm/aaaa, num objeto do tipo
     * Data.
     *
     * @param strData Data em formato de texto.
     * @return Data convertida em objeto.
     */
    public static Data converterString(String strData)
    {
        if (!verificarFormatoData(strData)) {
            throw new IllegalArgumentException("O formato não é válido.");
        }

        String[] data = strData.split("[-/]");

        int dia = Integer.parseInt(data[0]);
        int mes = Integer.parseInt(data[1]);
        int ano = Integer.parseInt(data[2]);

        return new Data(dia, mes, ano);
    }

    /**
     * Verificar se @param é uma Data válida
     *
     * @param strData String data a verificar
     * @return True se for válida
     */
    public static boolean verificarData(String strData)
    {
        if (verificarFormatoData(strData)) {
            Data data = converterString(strData);
            if (data.getMes() >= 1 && data.getDia() <= 12) {
                if (isAnoBissexto(data.getAno())) {
                    if (data.getMes() == 2)
                        if (data.getDia() >= 1 && data.getDia() <= 29)
                            return true;
                }
                if (data.getDia() >= 1 && data.getDia() <= diasPorMes[data.getMes()])
                    return true;
            }
        }
        return false;
    }
}
